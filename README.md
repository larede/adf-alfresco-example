# Application Development Framework (ADF)

https://www.alfresco.com/abn/adf/

É uma framework de componentes web (angular) para trablhar em cima do alfresco
usar este tipo de componentes pode ter vantagens:
- componentes à medida
- velocidade no desenvolvimento
MAS também tem desvantagens:
- ficas mais agarrado ao alfresco

# Projeto angular com um exemplo baseado em

Alfresco ADF Advanced Search Form Page
https://github.com/rjmfernandes/advancedsearch

## SSO com keycloak

Esta stack tem integrado um keycloak onde se encontra os utilizadores do alfresco. 
No passado nunca tinha conseguido SSO entre alfresco e keycloak via configurações nativas e agora já me ententi com isto (ver compose!!)

## Como utilizar

$ docker-compose up -d

http://localhost
larede@gmail.com / arede00