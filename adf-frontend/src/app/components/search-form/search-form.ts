export class SearchForm {
  constructor(
    public name?: string,
    public description?: string,
    public title?: string
  ) {}
}
