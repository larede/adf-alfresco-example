import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SearchForm } from './search-form';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent {
  model = new SearchForm(null, null, null);

  constructor(public router: Router) {}

  onSubmit() {
    let advancedQuery = this.getAdvancedQuery();
    if (advancedQuery != '') {
      this.router.navigate([
        '/search',
        {
          q: '*',
          aq: advancedQuery
        }
      ]);
    } else {
      this.router.navigate([
        '/search',
        {
          q: '*'
        }
      ]);
    }
  }

  getAdvancedQuery() {
    let aq = '';
    if (this.model.name != null && this.model.name != '') {
      aq = aq + 'cm:name:"' + this.model.name + '"';
    }
    if (this.model.title != null && this.model.title != '') {
      if (aq != '') {
        aq = aq + ',';
      }
      aq = aq + 'cm:title:"' + this.model.title + '"';
    }
    if (this.model.description != null && this.model.description != '') {
      if (aq != '') {
        aq = aq + ',';
      }
      aq = aq + 'cm:description:"' + this.model.description + '"';
    }
    return aq;
  }
}
